#ifndef ALPHA_TAG_STUDY
#define ALPHA_TAG_STUDY


#include "sstream"
#include "iostream"
#include "fstream"
#include "string.h"
#include "map"
#include "vector"
#include "math.h"
#include "TROOT.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TIterator.h"
#include "TKey.h"
#include "TClass.h"
#include "TTree.h"
#include "TChain.h"
#include "TDatime.h"
#include "TH1.h"
#include "TH2.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "boost/function.hpp"
#include "boost/algorithm/string/replace.hpp"
#include "boost/filesystem.hpp"
#include "extra_tools.cc"
#include "alpha_tag_study.h"
#include "get_ring_index.cc"

// #define DEBUG
#ifdef DEBUG
#define dout(x) std::cout << x <<std::endl
#else
#define dout(x) 
#endif

#define reportEvery 50000

alphatag_study::alphatag_study(std::string _ntuple_list_file, std::string _file_out, Bool_t _isData,
	Bool_t _make_t_graphs, std::string _time_bins_file, UInt_t _eventsPerBin,
	std::string _lumi_file, std::vector<UInt_t> _runFilter):
ntuple_list_file(_ntuple_list_file), file_out(_file_out), isData(_isData), make_t_graphs(_make_t_graphs),
time_bins_file(_time_bins_file), eventsPerBin(_eventsPerBin), lumi_file(_lumi_file), runFilter(_runFilter)
{
	std::cout << "[" << __LINE__ << "]" << "\t"  <<
	" Ntuples list: " << ntuple_list_file << std::endl << "\t" <<
	" Output file: " << file_out << std::endl << "\t"  <<
	" IsData: " << isData << std::endl << "\t"  <<
	" Make timed histograms: " << make_t_graphs << std::endl << "\t"  <<
	" Time bins file: " << time_bins_file << std::endl << "\t"  <<
	" Events PerBin: " << eventsPerBin << std::endl << "\t"  <<
	" Lumi file: " << lumi_file << std::endl << std::endl<< "\t"  <<
	" Run filter: ";
	for( auto & excludeRun : runFilter){
		std::cout<<excludeRun<<"  ";
	}
	std::cout<<std::endl<<std::endl;
	ntuple_list = getNonemptyLines(ntuple_list_file);
	Init_Map("eerings.dat");
	std::string anomalous_electrons_fileout = file_out;
	boost::replace_all(anomalous_electrons_fileout,".root","_anomalous_electrons.txt");
	anomalous_electrons.open (anomalous_electrons_fileout);
	for(auto & ntuple_path : ntuple_list ){
		anomalous_electrons<<ntuple_path<<std::endl;
	}
	anomalous_electrons<<std::endl<<std::endl;
	anomalous_electrons<<"ring,run,lumi,event,eIndex,eEta,ePhi,seedX,seedY"<<std::endl;
};


void alphatag_study::findTimeBins(){
	ntuple_chain = new TChain("selected");
	std::cout << "Adding ntuples:" << std::endl;
	for(auto & ntuple : ntuple_list){
		ntuple_chain->Add(ntuple.c_str());
		std::cout << "\t" << ntuple << std::endl;
	}
	begin_time = ntuple_chain->GetMinimum("eventTime");
	end_time = ntuple_chain->GetMaximum("eventTime");
	std::cout<<"Time limits:"<<std::endl<<"\tstart "<<TDatime(begin_time).AsSQLString()<<std::endl<<"\tstart "<<TDatime(end_time).AsSQLString()<<std::endl;
	readLumi();
	ntuple_reader = new TTreeReader(ntuple_chain);
	ntuple_reader->SetEntry(0);

	setNtuple(ntuple_reader);
	initializeRings();
	constructTimeHistograms();
	skimTree(ntuple_reader);

	for(auto & _ring : rings){

		optiIseTimeBinning(_ring.stats.hist);
	// //second pass
	// std::cout<<"\tSecond pass of bin optimization..."<<std::endl;
	// optiIseTimeBinning(_ring.stats.hist);

	}

	TFile *time_bins_fileout = new TFile(time_bins_file.c_str(), "RECREATE");
	time_bins_fileout->cd();
	for(auto & _ring : rings){
		_ring.stats.hist->Write();
	}

	lumi_hist->Write();
	time_bins_fileout->Close();
	std::cout<<"Time bins written to file "<<time_bins_file<<std::endl;
	closeTChain(ntuple_chain);
};


void alphatag_study::analyze(){

	ntuple_chain = new TChain("selected");

	std::cout << "Adding ntuples:" << std::endl;

	for(auto & ntuple : ntuple_list){

		ntuple_chain->Add(ntuple.c_str());
		std::cout << "\t" << ntuple << std::endl;
	}

	ntuple_reader = new TTreeReader(ntuple_chain);

	setNtuple(ntuple_reader);

	initializeRings();

	TFile *outfile = new TFile(file_out.c_str(), "RECREATE");

	setHistograms(outfile);

	analyzeTree(ntuple_reader);

	outfile->cd();

	for(auto & _ring : rings){
		for(auto & _1dhist : _ring.hists_1D){
			std::cout<<"\tWriting 1d histogram "<<_1dhist.hist->GetName()<<std::endl;
			_1dhist.hist->Write();

		}

		for(auto & _2dhist : _ring.hists_2D){
			std::cout<<"\tWriting 2d histogram "<<_2dhist.hist->GetName()<<std::endl;
			_2dhist.hist->Write();

		}

		//write anomalous region's histograms
		for(auto & _1dhist : _ring.anomalous_hists_1D){
			std::cout<<"\tWriting 1d histogram "<<_1dhist.hist->GetName()<<std::endl;
			_1dhist.hist->Write();

		}

		for(auto & _2dhist : _ring.anomalous_hists_2D){
			std::cout<<"\tWriting 2d histogram "<<_2dhist.hist->GetName()<<std::endl;
			_2dhist.hist->Write();

		}

		if(make_t_graphs) {

			for(auto & _timedhist : _ring.hists_timed){
				std::cout<<"\tWriting timed histogram "<<_timedhist.hist->GetName()<<std::endl;
				_timedhist.hist->Write();

			}

			_ring.stats.hist->Write();

		}

	}

	drawRingMap();
	ringMap_hist->Write();
	outfile->Close();
	std::cout<<"Task Complete!"<<std::endl<<"Histograms written to "<<file_out<<std::endl;

	anomalous_electrons.close();
};


void alphatag_study::setHistograms(TFile *_writeFile){

	std::cout<<"Initializing histograms:"<<std::endl;

	for(auto & _ring : rings){

		setRingHistograms(_ring, _writeFile);

	}

};


void alphatag_study::analyzeTree(TTreeReader *_ntuple_reader){

	while (_ntuple_reader->Next()) {

		Long64_t current_entry = _ntuple_reader->GetCurrentEntry();
		if(current_entry % reportEvery == 0 ) std::cout<<"\tProcessing entry "<<current_entry<<std::endl;

		if(!applyCuts()) continue;

		for(auto & _ring : rings){

			if(!isInRing(_ring)) continue;

			assignRingVars();

			fillRingHistograms(_ring);

		}

	}

};


void alphatag_study::setRingHistograms(ring &_ring, TFile *_writeFile){

	//create 1d hists
	for(auto & _1dvar : oneD_hists){

		_ring.hists_1D.emplace_back(*_1dvar);

	}

	for (auto & _1dhists : _ring.hists_1D){

		_1dhists.initializehist(_ring.name, _ring.title, _writeFile);

	}

	//create 2d hists
	for(auto & _2dvars : twoD_hists){

		_ring.hists_2D.emplace_back(*_2dvars.first, *_2dvars.second);

	}

	for(auto & _2dhists : _ring.hists_2D){
		_2dhists.initializehist(_ring.name, _ring.title, _writeFile);
	}


	//create histograms for anomalous eta region
	for(auto & _1dvar : oneD_hists){

		_ring.anomalous_hists_1D.emplace_back(*_1dvar);

	}

	for (auto & _1dhists : _ring.anomalous_hists_1D){

		_1dhists.initializehist(_ring.name+"_anom", _ring.title+" anom", _writeFile);

	}

	//create 2d hists
	for(auto & _2dvars : twoD_hists){

		_ring.anomalous_hists_2D.emplace_back(*_2dvars.first, *_2dvars.second);

	}

	for(auto & _2dhists : _ring.anomalous_hists_2D){
		_2dhists.initializehist(_ring.name+"_anom", _ring.title+" anom", _writeFile);
	}


	//create timed histograms
	if(!make_t_graphs) return;
	std::string time_bins_histname = removeNonAlpha(_ring.name) + "_1D_" + "Time";

	TH1D* time_bins_hist = (TH1D*) getHistFromFile(time_bins_histname, time_bins_file);

	std::vector<Double_t> time_bins = getXbins(time_bins_hist);

	time_bins_hist->Delete();

	_ring.time_axis.set(eventTime, time_bins.data(), time_bins.size()-1, "Time");
	std::cout << "\t\t# of bins  in " << time_bins_histname << " : " << time_bins.size()-1<< std::endl;

	_ring.stats = histogram_template(_ring.time_axis);

	_ring.stats.initializehist(_ring.name, _ring.title, _writeFile);

	_ring.stats.hist->SetTitle(_ring.title.c_str());
	_ring.stats.hist->SetDrawOption("HIST");

	for(auto & timed_var : timed_hists){

		_ring.hists_timed.emplace_back(_ring.time_axis, *timed_var);

	}

	for(auto & timed_hist : _ring.hists_timed){

		timed_hist.initializehist(_ring.name, _ring.title, _writeFile);

		timed_hist.hist->GetXaxis()->SetTimeDisplay(1);

		timed_hist.hist->GetXaxis()->SetTimeFormat("#splitline{%d-%b}{%H:%M}%F1970-01-01 00:00:00s0");

	}

	delete [] _ring.time_axis.xBins;

};


void alphatag_study::readLumi(){
	std::cout << "[" << __LINE__ << "]" << " Reading lumi file..." << std::endl;
	CSVReader reader(lumi_file);

	std::vector<std::vector<std::string> > data = reader.getData();

	UInt_t low_edge;
	UInt_t fillNum;

	UInt_t first_row = 4;

	for (UInt_t it = 2; it < data.size() - 3; it++) {

		try {

			low_edge = std::stoi(data[it][2]);

		} catch (...) {

			std::cout << "[" << __LINE__ << "]" << " Error reading " << lumi_file << " in line" << it << std::endl;

			break;
		}

		if (low_edge >= begin_time - max_bin_width) {

			first_row = it;

			break;
		}

	}

	std::vector<Double_t> bins, bin_contents;

	bins.reserve(data.size() + 1);

	bin_contents.reserve(data.size() + 1);

	Double_t content, fillLum = 0;

	low_edge = std::stoi(data[first_row][2]);

	fillNum = std::stoi(data[first_row][0].erase(0, 7));

	fillMap[low_edge] = fillNum;
	std::cout << "[" << __LINE__ << "]" << " Fill info:" << std::endl;
	std::cout << "\t" << "Time" << "\t\t\t" << "Fill" << "\t" << "Luminosity[/pb]" << std::endl;
	for (UInt_t it = first_row + 1; it < data.size() - 3; it++) {

		try {

			low_edge = std::stoi(data[it][2]);

			fillNum = std::stoi(data[it][0].erase(0, 7));

			content = std::stod(data[it][6]);

			fillLum += content;


		} catch (...) {
			std::cout << "[" << __LINE__ << "]" << " Error reading " << lumi_file << " in line" << it << std::endl;
			break;
		}

		bin_contents.push_back(content);

		bins.push_back(low_edge);

		if (fillMap.rbegin()->second < fillNum) {

			fillMap[low_edge] = fillNum;

			auto rit = fillMap.rbegin();

			rit++;
			std::cout << "\t" << TDatime(rit->first).AsSQLString()  << "\t" << rit->second << "\t" << fillLum / 1000000 << std::endl;
			fillLum = 0;

		}

	}

	lumi_hist = new TH1D("Luminosity", "Luminosity", bins.size() - 1, bins.data());

	for (UInt_t i = 0; i < lumi_hist->GetNbinsX(); i++) {

		lumi_hist->SetBinContent(i + 1, bin_contents[i] / 1000000000.);

	}

	lumi_hist->Sumw2();

	lumi_hist->GetXaxis()->SetTimeDisplay(1);
	lumi_hist->GetXaxis()->SetTimeFormat("#splitline{%d-%b}{%H:%M}%F1970-01-01 00:00:00s0");
	lumi_hist->GetXaxis()->SetNdivisions(510, kFALSE);

	lumi_hist->Scale(1000000, "width");

	lumi_hist->GetYaxis()->SetTitle("Luminosity [10^{33} cm^{-2} s^{-1}]");
	lumi_hist->GetYaxis()->CenterTitle();

	lumi_hist->SetMarkerSize(0);
	lumi_hist->SetStats(0);
	lumi_hist->SetDrawOption("bar hist");
	lumi_hist->SetMinimum(0.000001);
	lumi_hist->SetMaximum(30);

	lumi_hist->GetXaxis()->SetRangeUser(begin_time, end_time);

};


UInt_t alphatag_study::findFill(UInt_t t) {

	for (auto & it : fillMap) {

		if (it.first >= t) {

			return it.second;

		}

	}
	cout << "[" << __LINE__ << "]"  << " Fill NOT found for time " << t << endl;
	return 0;
};


Bool_t alphatag_study::inSameFill(Double_t t1, Double_t t2) {

	UInt_t _t1 = std::round(t1);

	UInt_t _t2 = std::round(t2);

	UInt_t fill1 = findFill(_t1);

	UInt_t fill2 = findFill(_t2);

	Bool_t IsSameFill = (fill1 == fill2) || (fill1 == 0) || (fill2 == 0);

	return IsSameFill;
};


void alphatag_study::initializeRings(){

	rings.clear();
	std::cout<<"Constructing ring placeholders:"<<std::endl;

	for(UInt_t ringIndex = 0; ringIndex < nrings; ringIndex++){

		std::string ringTitle = "\\bigcirc\\ \\ " + std::to_string(ringIndex);
		std::string ringName = "ring_" + std::to_string(ringIndex);

		rings.emplace_back(ringName, ringTitle, ringIndex);
		std::cout<<"\t\t"<<ringName<<std::endl;
	}

};


void alphatag_study::constructTimeHistograms(){
	std::cout<<"Constructing time histograms for rings:"<<std::endl;
	for(auto & _ring : rings){

		_ring.time_axis = plot_variable(eventTime, (Double_t) begin_time, (Double_t) end_time, end_time - begin_time, "Time");

		_ring.stats = histogram_template(_ring.time_axis);

		_ring.stats.initializehist(_ring.name, _ring.title, 0);

		_ring.stats.hist->SetTitle(_ring.title.c_str());

	}

};


void alphatag_study::setNtuple(TTreeReader *_ntuple_reader){
	_eventTime.set(*_ntuple_reader, "eventTime");
	_runNumber.set(*_ntuple_reader, "runNumber");
	_eventNumber.set(*_ntuple_reader, "eventNumber");
	_lumiBlock.set(*_ntuple_reader, "lumiBlock");
	if(!isData)_mcGenWeight.set(*_ntuple_reader, "mcGenWeight");
	_invMass.set(*_ntuple_reader, "invMass");
	_eleID.set(*_ntuple_reader, "eleID");
	// _energy_ele.set(*_ntuple_reader, "energy_ele");
	_energy_ele.set(*_ntuple_reader, "rawEnergySCEle");
	_energy_ECAL_ele.set(*_ntuple_reader, "energy_ECAL_ele");
	// _energy_ECAL_ele.set(*_ntuple_reader, "rawEnergySCEle");
	_rawEnergySCEle.set(*_ntuple_reader, "rawEnergySCEle");
	_R9Ele.set(*_ntuple_reader, "R9Ele");
	_etaEle.set(*_ntuple_reader, "etaEle");
	_etaSCEle.set(*_ntuple_reader, "etaSCEle");
	_phiSCEle.set(*_ntuple_reader, "phiSCEle");
	_phiEle.set(*_ntuple_reader, "phiEle");
	// _ringSCseed.set(*_ntuple_reader, "ringSCseed");
	_xSeedSC.set(*_ntuple_reader, "xSeedSC");
	_ySeedSC.set(*_ntuple_reader, "ySeedSC");
	_alphaSeedSC.set(*_ntuple_reader, "alphaSeedSC");
	_pAtVtxGsfEle.set(*_ntuple_reader, "pAtVtxGsfEle");

};


void alphatag_study::skimTree(TTreeReader* _ntuple_reader){
	std::cout<<"Scanning events..."<<std::endl;
	while (_ntuple_reader->Next()) {

		Long64_t current_entry = _ntuple_reader->GetCurrentEntry();

		if(current_entry % reportEvery == 0 ) std::cout<<"\tProcessing entry "<<current_entry<<std::endl;
		if(!applyCuts()) continue;

		for(auto & _ring : rings){

			if(!isInRing(_ring)) continue;

	// cout<<"selected "<<current_entry<<" "<<*(_ring.stats.var)<<" "<<eventTime<< endl;
			_ring.stats.fill();

		}

	}

};


void alphatag_study::optiIseTimeBinning(TH1D *&tHist){
	if(!tHist){
		std::cout<<"Error! Cannot optimize null histogram!"<<std::endl;
		return;
	}

	std::cout <<"\tOptimising time bins for " << tHist->GetName() << std::endl;
	std::vector<Double_t> bins;
	std::vector<Double_t> contents;

	UInt_t firstBin = tHist->FindFirstBinAbove(0, 1), lastBin = tHist->FindLastBinAbove(0, 1);
	bins.push_back(tHist->GetXaxis()->GetBinLowEdge(firstBin));

	Double_t binSum = 0.;

	for (UInt_t j = firstBin; j < lastBin + 1; j++) {
		binSum += tHist->GetBinContent(j);
		UInt_t lowEdge = round(bins.back());
		UInt_t upEdgeCand = round(tHist->GetXaxis()->GetBinUpEdge(j));
		Bool_t metThreshold = (binSum >= eventsPerBin);

		Bool_t tooWideBin = (upEdgeCand - lowEdge > max_bin_width);

	// Bool_t differentFill = !inSameFill(lowEdge, upEdgeCand);

		Bool_t insertBin = metThreshold || tooWideBin;

		if (insertBin){

			bins.push_back(upEdgeCand);

			contents.push_back(binSum);

			binSum = 0;

		}

		Int_t lastBinEdge = bins.size()-1;

		Bool_t mergeTwoBins = 0;

		if(lastBinEdge>2) mergeTwoBins = (contents[lastBinEdge-2] > 0.5) && ((contents[lastBinEdge-3] > 0.5) || (contents[lastBinEdge-1] > 0.5))
			&& (contents[lastBinEdge-2] < binMergeThreshold * eventsPerBin);

		if(mergeTwoBins){
			Int_t mergeWhich = (contents[lastBinEdge-3] < contents[lastBinEdge-1]) ? 2 : 1;

			bins.erase(bins.begin()+(lastBinEdge-mergeWhich));

			contents[lastBinEdge-mergeWhich] += contents[lastBinEdge-mergeWhich-1];

			contents.erase(contents.begin()+(lastBinEdge-mergeWhich-1));

		}
	}

	if (bins.back() < tHist->GetXaxis()->GetBinUpEdge(lastBin)) {

		bins.push_back(tHist->GetXaxis()->GetBinUpEdge(lastBin));

	};

	TH1D* outHist = new TH1D("", "", bins.size() - 1, bins.data());

	for (UInt_t i = 0; i < contents.size(); i++) {

		outHist->SetBinContent(i + 1.01, contents[i]);

	};

	outHist->GetXaxis()->CenterTitle(true);
	outHist->GetXaxis()->SetTimeDisplay(1);
	outHist->GetXaxis()->SetLabelOffset(0.03);;
	outHist->GetXaxis()->SetTimeFormat("#splitline{%d-%b}{%H:%M}%F1970-01-01 00:00:00s0");
	outHist->GetYaxis()->CenterTitle(true);
	outHist->GetYaxis()->SetTitle("# of events");

	std::string name = tHist->GetName();
	std::string title = tHist->GetTitle();

	tHist->Delete();

	tHist = NULL;
	outHist->SetName(name.c_str());
	outHist->SetTitle(title.c_str());
	tHist = outHist;
	std::cout <<"\t"<< name << " : number of bins = " << bins.size() - 1 << std::endl;
};


Bool_t alphatag_study::applyCuts(){
	mPassBasicCuts = 0;
	//apply run filter
	if(!runFilter.empty()){
	//initialize run number
		runNumber = _runNumber;
		if(std::find(runFilter.begin(), runFilter.end(), runNumber)!= runFilter.end()) return mPassBasicCuts;
	}
	//require at least one electron to be in EE
	ele0inEE = std::abs(_etaSCEle[0]) > BETRetaMin;
	ele1inEE = std::abs(_etaSCEle[1]) > BETRetaMin;
	zSide_ele0 = (_etaSCEle[0] > 0) ? +1 : -1;
	zSide_ele1 = (_etaSCEle[1] > 0) ? +1 : -1;

	if( !ele0inEE && !ele1inEE) return mPassBasicCuts;

	//apply loose ID
	if (!( _eleID[0] & eleIDmask)) return mPassBasicCuts;
	if (!(_eleID[1] & eleIDmask)) return mPassBasicCuts;

	//Et cut
	Float_t Et0 = _energy_ECAL_ele[0] / std::cosh(_etaEle[0]);
	if (Et0 < EtLowLimit) return mPassBasicCuts;

	Float_t Et1 = _energy_ECAL_ele[0] / std::cosh(_etaEle[0]);
	if (Et1 < EtLowLimit) return mPassBasicCuts;

	invMass = _invMass;
	//60 GeV Z-mass window cut

	if( invMass < 60. || invMass > 120.) return mPassBasicCuts;

	//initialize event time
	eventTime = 0.1 + (Double_t)_eventTime;

	if(isData) mcGenWeight = 1.;
	else mcGenWeight = _mcGenWeight;
	mPassBasicCuts = 1;

	return mPassBasicCuts;
};


Bool_t alphatag_study::isInRing(ring & _ring){

	m_passRing = 0;

	currentRing = _ring.ringIndex;

	// Bool_t ele0inRing = (std::abs(_ringSCseed[0]) == currentRing);
	Bool_t ele0inRing = (ele0inEE && std::abs(getRingIndex(_xSeedSC[0], _ySeedSC[0], zSide_ele0)) == currentRing);

	// Bool_t ele1inRing = (std::abs(_ringSCseed[1]) == currentRing);
	Bool_t ele1inRing = (ele1inEE && std::abs(getRingIndex(_xSeedSC[1], _ySeedSC[1], zSide_ele1)) == currentRing);

	if(ele0inRing && !ele1inRing) probeIndex = 0;
	else if(!ele0inRing && ele1inRing) probeIndex = 1;
	else if(!ele0inRing && !ele1inRing) return m_passRing;
	else _rawEnergySCEle[0] > _rawEnergySCEle[1] ? (probeIndex = 0) : (probeIndex = 1);

	zSide_probe = (probeIndex == 0) ? zSide_ele0 : zSide_ele1;

	m_passRing = 1;

	return m_passRing;
};


void alphatag_study::assignRingVars(){
	tagIndex = 1 - probeIndex;

	TLorentzVector tagP4corrected;
	Float_t tagPtcorrected = _energy_ele[tagIndex] / std::cosh(_etaEle[tagIndex]);
	tagP4corrected.SetPtEtaPhiE(tagPtcorrected, _etaEle[tagIndex], _phiEle[tagIndex], _energy_ele[tagIndex]);


	TLorentzVector tagP4mustache;
	Float_t tagPtmustache = _energy_ECAL_ele[tagIndex] / std::cosh(_etaEle[tagIndex]);
	tagP4mustache.SetPtEtaPhiE(tagPtmustache, _etaEle[tagIndex], _phiEle[tagIndex], _energy_ECAL_ele[tagIndex]);

	TLorentzVector probeP4corrected;
	Float_t probePtcorrected = _energy_ele[probeIndex] / std::cosh(_etaEle[probeIndex]);
	probeP4corrected.SetPtEtaPhiE(probePtcorrected, _etaEle[probeIndex], _phiEle[probeIndex], _energy_ele[probeIndex]);

	TLorentzVector probeP4mustache;
	Float_t probePtmustache = _energy_ECAL_ele[probeIndex] / std::cosh(_etaEle[probeIndex]);
	probeP4mustache.SetPtEtaPhiE(probePtmustache, _etaEle[probeIndex], _phiEle[probeIndex], _energy_ECAL_ele[probeIndex]);

	invMassTnP = (tagP4corrected + probeP4mustache).M();

	invMassCorrected = (tagP4corrected + probeP4corrected).M();

	invMassTnP_over_invMassCorrected = invMassTnP / invMassCorrected;

	invMassMustache = (tagP4mustache + probeP4mustache).M();

	R9_tag = _R9Ele[tagIndex];

	R9_probe = _R9Ele[probeIndex];

	eta_tag = _etaEle[tagIndex];

	eta_probe = _etaEle[probeIndex];

	phi_tag = _phiEle[tagIndex];

	phi_probe = _phiEle[probeIndex];

	etaSC_probe = _etaSCEle[probeIndex];

	phiSC_probe = _phiSCEle[probeIndex];

	xSeedSC_probe = zSide_probe * _xSeedSC[probeIndex];

	ySeedSC_probe = _ySeedSC[probeIndex];

	energy_mustache_tag = _energy_ECAL_ele[tagIndex];

	energy_mustache_probe = _energy_ECAL_ele[probeIndex];

	rawEnergy_SC_tag = _rawEnergySCEle[tagIndex];

	rawEnergy_SC_probe = _rawEnergySCEle[probeIndex];

	energy_corrected_tag = _energy_ele[tagIndex];

	energy_corrected_probe = _energy_ele[probeIndex];

	energy_must_over_corr_tag = energy_mustache_tag / energy_corrected_tag;

	energy_must_over_corr_probe = energy_mustache_probe / energy_corrected_probe;

	alpha_seedSC_probe = _alphaSeedSC[probeIndex];

	pT_Z = (tagP4mustache + probeP4mustache).Pt();

	dR_tag_probe = tagP4mustache.DeltaR(probeP4mustache);

	dPhi_tag_probe = std::abs(_phiEle[tagIndex]-_phiEle[probeIndex]);

	dEta_tag_probe = std::abs(_etaEle[tagIndex] - _etaEle[probeIndex]);

	EoverP_probe = _energy_ECAL_ele[probeIndex]/_pAtVtxGsfEle[probeIndex];
};


Bool_t alphatag_study::isInAnomalousRegion(Double_t _eta){
	if(_eta >1.4 && _eta <1.5 )return 1;
	return 0;
};


void alphatag_study::fillRingHistograms(ring & _ring){

	for(auto & _1dhist : _ring.hists_1D){

		_1dhist.fill(mcGenWeight);

	}

	for(auto & _2dhist : _ring.hists_2D){

		_2dhist.fill(mcGenWeight);

	}

	if(isInAnomalousRegion(eta_probe)){

		anomalous_electrons<< _ring.ringIndex<<","<<_runNumber<<","<<_lumiBlock<<","<<
		_eventNumber<<","<<probeIndex<<","<<eta_probe <<","<<phi_probe<<","<<xSeedSC_probe<<","<<ySeedSC_probe<<std::endl;

		for(auto & _1dhist : _ring.anomalous_hists_1D){

			_1dhist.fill(mcGenWeight);

		}

		for(auto & _2dhist : _ring.anomalous_hists_2D){

			_2dhist.fill(mcGenWeight);

		}
	}

	if(!make_t_graphs) return;

	_ring.stats.fill();

	for(auto & _timedhist : _ring.hists_timed){

		_timedhist.fill();

	}

};


void alphatag_study::drawRingMap(){
	ringMap_hist = new TH2D("ring_map", "\\bigcirc\\ \\ map;i_{x};i_{y}", 201, -101.,100., 100, 0., 100.);
	for(auto & mapEntry : ring_map){
		std::tuple<Int_t, Int_t, Int_t> _coordinates(mapEntry.first);
		Int_t binNumber = ringMap_hist->FindBin(std::get<2>(_coordinates) * std::get<0>(_coordinates), std::get<1>(_coordinates));
		ringMap_hist->SetBinContent(binNumber, std::abs(mapEntry.second) + 1);
	}
};


alphatag_study::~alphatag_study(){};

#endif
