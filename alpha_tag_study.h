#define nrings 39
#define lumisection 24
#define max_bin_width 24.*3600.
#define BETRetaMin 1.4442
#define BETRetaMax 1.566
#define BEetaBoundary 1.479
#define eleIDmask 0x0004 //loose94XV1Run2017
#define EtLowLimit 20.0
#define binMergeThreshold 0.5

#define initvar 999


struct ring{
	std::string name;
	std::string title;
	Int_t ringIndex;
	ring(std::string _name, std::string _title, Int_t _ringIndex){
		set(_name, _title, _ringIndex);
	};
	ring(){};
	~ring(){};
	void set(std::string _name, std::string _title, Int_t _ringIndex){
		name=_name;
		title=_title;
		ringIndex=_ringIndex;
	};
	plot_variable time_axis; //call delete [] this->time_axis.xBins after constructing timed histograms
	histogram_template stats;
	std::vector<histogram_template> hists_1D;
	std::vector<twoDhistogram_template> hists_2D;
	std::vector<twoDhistogram_template> hists_timed;

	std::vector<histogram_template> anomalous_hists_1D;
	std::vector<twoDhistogram_template> anomalous_hists_2D;
};


class alphatag_study {
public:
	alphatag_study(std::string _ntuple_list_file, std::string _file_out, Bool_t _isData,
		Bool_t _make_t_graphs, std::string _time_bins_file, UInt_t _eventsPerBin,
		std::string _lumi_file="", std::vector<UInt_t> _runFilter = {});

	~alphatag_study();
	void findTimeBins();
	void analyze();

private:
	std::string ntuple_list_file;
	Bool_t make_t_graphs;
	Bool_t isData; 
	std::string time_bins_file;
	std::string lumi_file;
	std::string file_out;
	UInt_t eventsPerBin;
	std::vector<UInt_t> runFilter;

	std::vector<std::string> ntuple_list;
	UInt_t begin_time, end_time;

	ofstream anomalous_electrons;

	void drawRingMap();
	TH2D *ringMap_hist = nullptr;

	void readLumi();
	TH1D* lumi_hist = NULL;
	std::map<UInt_t, UInt_t> fillMap;
	UInt_t findFill(UInt_t t);
	Bool_t inSameFill(Double_t t1, Double_t t2);

	void constructTimeHistograms();
	void skimTree(TTreeReader* _ntuple_reader);
	void optiIseTimeBinning(TH1D *&tHist);

	TChain *ntuple_chain = nullptr;
	TTreeReader *ntuple_reader = nullptr;
	TTreeReaderAnyValue<UInt_t> _eventTime;
	TTreeReaderAnyValue<UInt_t> _runNumber;
	TTreeReaderAnyValue<ULong64_t> _eventNumber;
	TTreeReaderAnyValue<UShort_t> _lumiBlock;
	TTreeReaderAnyValue<Float_t> _mcGenWeight;
	TTreeReaderAnyValue<Float_t> _invMass;
	TTreeReaderArrayValue<UInt_t> _eleID;
	TTreeReaderArrayValue<Float_t> _energy_ele;
	TTreeReaderArrayValue<Float_t> _energy_ECAL_ele;
	TTreeReaderArrayValue<Float_t> _rawEnergySCEle;
	TTreeReaderArrayValue<Float_t> _pAtVtxGsfEle;
	TTreeReaderArrayValue<Float_t> _R9Ele;
	TTreeReaderArrayValue<Float_t> _etaEle;
	TTreeReaderArrayValue<Float_t> _phiEle;
	TTreeReaderArrayValue<Float_t> _etaSCEle;
	TTreeReaderArrayValue<Float_t> _phiSCEle;
	TTreeReaderArrayValue<Int_t> _ringSCseed;
	TTreeReaderArrayValue<Float_t> _alphaSeedSC;
	TTreeReaderArrayValue<Short_t> _xSeedSC;
	TTreeReaderArrayValue<Short_t> _ySeedSC;

	void initializeRings();
	std::vector<ring> rings;

	void setHistograms(TFile *_writeFile);
	void setRingHistograms(ring &_ring, TFile *_writeFile);
	void analyzeTree(TTreeReader *_ntuple_reader);
	void setNtuple(TTreeReader *_ntuple_reader);
	Bool_t applyCuts();
	Bool_t mPassBasicCuts = 0;

	Double_t eventTime = initvar;
	UInt_t runNumber = initvar;
	Double_t invMass = -initvar;
	Float_t mcGenWeight = -initvar;

	Bool_t ele0inEE;
	Bool_t ele1inEE;
	Int_t zSide_ele0;
	Int_t zSide_ele1;
	

	Bool_t isInRing(ring & _ring);
	void assignRingVars();
	void fillRingHistograms(ring & _ring);
	Bool_t isInAnomalousRegion(Double_t _eta);

	Bool_t m_passRing = 0;
	Int_t currentRing = -initvar;
	Int_t tagIndex = -initvar;
	Int_t probeIndex = -initvar;
	Double_t invMassTnP = -initvar;
	Double_t invMassCorrected = -initvar;
	Double_t invMassTnP_over_invMassCorrected = -initvar;
	Double_t invMassMustache = -initvar;
	Double_t pT_Z = -initvar;
	Double_t R9_tag = -initvar;
	Double_t R9_probe = -initvar;
	Double_t eta_tag = -initvar;
	Double_t eta_probe = -initvar;
	Double_t dR_tag_probe = -initvar;
	Double_t dEta_tag_probe = -initvar;
	Double_t dPhi_tag_probe = -initvar;
	Double_t phi_tag = -initvar;
	Double_t phi_probe = -initvar;
	Double_t etaSC_probe = -initvar;
	Double_t phiSC_probe = -initvar;
	Double_t xSeedSC_probe = -initvar;
	Double_t ySeedSC_probe = -initvar;
	Double_t rawEnergy_SC_tag = -initvar;
	Double_t rawEnergy_SC_probe = -initvar;
	Double_t energy_mustache_tag = -initvar;
	Double_t energy_mustache_probe = -initvar;
	Double_t energy_corrected_tag = -initvar;
	Double_t energy_corrected_probe = -initvar;
	Double_t energy_must_over_corr_tag = -initvar;
	Double_t energy_must_over_corr_probe = -initvar;
	Double_t alpha_seedSC_probe = -initvar;
	Double_t zSide_probe = -initvar;
	Double_t EoverP_probe = -initvar;

	plot_variable
	var_invMassTnP{invMassTnP, 60., 120., 600, "m_{ee}^{T\\&P}", "GeV"},
	var_invMassCorrected{invMassCorrected, 60., 120., 600, "m_{ee}^{corr.}", "GeV"},
	var_invMassTnP_over_invMassCorrected{invMassTnP_over_invMassCorrected, 0., 2., 200, "m_{ee}^{T\\&P}/m_{ee}^{corr.}"},
	var_invMassMustache{invMassMustache, 60., 120., 600, "m_{ee}^{must.}", "GeV"},
	var_pT_Z{pT_Z, 0., 300., 300, "p_{T}(Z)", "GeV"},
	var_R9_tag{R9_tag, 0., 1., 100, "R_{9}\\ (tag)"},
	var_R9_probe{R9_probe, 0., 1., 100, "R_{9}\\ (probe)"},
	// var_eta_tag{eta_tag, eta_binning.data(), eta_binning.size()-1, "\\eta\\ (tag)"},
	// var_eta_probe{eta_probe, eta_binning.data(), eta_binning.size()-1, "\\eta\\ (probe)"},
	var_eta_tag{eta_tag, -3., 3., 600, "\\eta\\ (tag)"},
	var_eta_probe{eta_probe, -3., 3., 600, "\\eta\\ (probe)"},
	var_dR_tag_probe{dR_tag_probe, 0., 4., 400, "\\Delta R(tag, probe)"},
	var_dPhi_tag_probe{dPhi_tag_probe, 0., 3.15, 315, "\\Delta \\phi(tag, probe)"},
	var_dEta_tag_probe{dEta_tag_probe, 0.0, 4.0, 400, "\\Delta \\eta(tag, probe)"},
	var_phi_tag{phi_tag, -3.15, 3.15, 630, "\\phi\\ (tag)"},
	var_phi_probe{phi_probe, -3.15, 3.15, 630, "\\phi\\ (probe)"},
	var_etaSC_probe{etaSC_probe, -3., 3., 600, "\\eta^{SC}\\ (probe)"},
	var_phiSC_probe{phiSC_probe, -3.15, 3.15, 630, "\\phi^{SC}\\ (probe)"},
	var_xSeedSC_probe{xSeedSC_probe, -101., 101., 202, "i_{x}^{seedSC}\\ (probe)"},
	var_ySeedSC_probe{ySeedSC_probe, 0., 101., 101, "i_{y}^{seedSC}\\ (probe)"},
	var_rawEnergy_SC_tag{rawEnergy_SC_tag, 0., 1000., 500, "E^{rawSC}\\ (tag)", "GeV"},
	var_rawEnergy_SC_probe{rawEnergy_SC_probe, 0., 1000., 500, "E^{rawSC}\\ (probe)", "GeV"},
	var_energy_mustache_tag{energy_mustache_tag, 0., 1000., 500, "E^{must.}\\ (tag)", "GeV"},
	var_energy_mustache_probe{energy_mustache_probe, 0., 1000., 500, "E^{must.}\\ (probe)", "GeV"},
	var_energy_corrected_tag{energy_corrected_tag, 0., 1000., 500, "E^{corr.}\\ (tag)", "GeV"},
	var_energy_corrected_probe{energy_corrected_probe, 0., 1000., 500, "E^{corr.}\\ (probe)", "GeV"},
	var_energy_must_over_corr_tag{energy_must_over_corr_tag, 0., 2., 200, "E^{must}/E^{corr} \\ (tag)"},
	var_energy_must_over_corr_probe{energy_must_over_corr_probe, 0., 2., 200, "E^{must}/E^{corr}\\ (probe)"},
	var_alpha_seedSC_probe{alpha_seedSC_probe, 0., 2.0, 2000, "\\alpha^{seedSC}\\ (probe)"},
	var_EoverP_probe{EoverP_probe,0., 2., 2000, "E/p\\ (probe)"};

	std::vector<plot_variable*> oneD_hists = {
		// &var_invMassTnP,
		// &var_invMassCorrected,
		&var_invMassMustache,
		// &var_R9_tag,
		// &var_R9_probe,
		&var_eta_tag,
		&var_eta_probe,
		&var_etaSC_probe
		// &var_rawEnergy_SC_tag,
		// &var_rawEnergy_SC_probe,
		// &var_phi_tag,
		// &var_phi_probe,
		// &var_energy_mustache_tag,
		// &var_energy_mustache_probe,
		// &var_energy_corrected_tag,
		// &var_energy_corrected_probe,
		// &var_energy_must_over_corr_tag,
		// &var_energy_must_over_corr_probe,
		// &var_alpha_seedSC_probe
	};

	std::vector<std::pair<plot_variable*, plot_variable*>> twoD_hists = {
		// {&var_invMassTnP, &var_invMassCorrected},
		// {&var_energy_mustache_probe, &var_energy_corrected_probe},
		{&var_xSeedSC_probe, &var_ySeedSC_probe},
		{&var_etaSC_probe, &var_phiSC_probe},
		{&var_eta_probe, &var_eta_tag},
		{&var_phiSC_probe, &var_invMassMustache},
		{&var_phiSC_probe, &var_EoverP_probe}		
		// {&var_pT_Z, &var_dR_tag_probe},
		// {&var_pT_Z, &var_dPhi_tag_probe},
		// {&var_pT_Z, &var_dEta_tag_probe}
	};

	std::vector<plot_variable*> timed_hists = {
		&var_invMassTnP,
		&var_invMassCorrected,
		// &var_invMassMustache,
		&var_R9_probe,
		&var_energy_must_over_corr_probe		
	};
};