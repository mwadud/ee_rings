#include <sstream>
#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>




/****************************************************************/
/*	Get EE ring number from sypercrystal (x, y, z) coordinates	*/
/*  First, initialize the map by calling Init_Map()				*/
/*	getRingIndex() may now be called to find ring number		*/
/****************************************************************/
std::map<std::tuple<Int_t, Int_t, Int_t>, Int_t> ring_map;
void Init_Map(std::string _map_file_path){
	//load map file
	std::ifstream read_map(_map_file_path);

	std::cout<<"Reading ring map:"<<std::endl;

	//read each line
	std::string line;
	while (std::getline(read_map, line)){
		//erase spaces " "
		boost::replace_all(line, " ","");
		std::cout<<line<<"   ";

		//find position of ")""
		size_t pos_right_parenthesis = line.find_last_of(")");

		//skip line if not found
		if(pos_right_parenthesis == std::string::npos) continue;

		//extract coordinate string
		std::string coordinate_string = line.substr(1, pos_right_parenthesis-1);

		//split coordinate string and place coordinates in vector
		std::vector<string> split_coordinate_string;
		boost::split(split_coordinate_string, coordinate_string, boost::is_any_of(","));

		//create 3-tuple of coordinates
		Int_t x = std::stoi(split_coordinate_string[0]);
		Int_t y = std::stoi(split_coordinate_string[1]);
		Int_t z = std::stoi(split_coordinate_string[2]);
		std::tuple<Int_t, Int_t, Int_t> coordinates(x, y, z);

		//read ring value
		std::string ring_string = line.substr(pos_right_parenthesis+1);
		Int_t ringIndex = std::stoi(ring_string);
		//add EE+/- info as sign
		ringIndex = z > 0 ? std::abs(ringIndex) : -std::abs(ringIndex);

		ring_map[coordinates] = ringIndex;
		std::cout<<"\t( "<<x<<", "<<y<<", "<<z<<")"<<" "<<ring_map[coordinates]<<std::endl;
	}
};

Int_t getRingIndex(Int_t _x, Int_t _y, Int_t _z){
	std::tuple<Int_t, Int_t, Int_t> _coordinates(_x, _y, _z);
	if(ring_map.find(_coordinates) == ring_map.end()){
		// std::cout<<"\tRing index not found for coordinates ("<<_x<<", "<<_y<<", "<<_z<<") !" <<std::endl;
		return -999;
	};
	Int_t _ringIndex = ring_map[{_x, _y, _z}];
	return _ringIndex;
};